#!/bin/sh

# Move old user config (i.e. using -test switch) to a stable location
if [ -d "$HOME/.jajuk_test_1.8" -a ! -e "$HOME/.jajuk" ]; then
       	mv "$HOME/.jajuk_test_1.8" "$HOME/.jajuk"
fi
if [ -d "$HOME/.jajuk_test_1.7" -a ! -e "$HOME/.jajuk" ]; then
	mv "$HOME/.jajuk_test_1.7" "$HOME/.jajuk"
fi

JAVA_OPTS="-client -Xms20M -Xmx512M -XX:MinHeapFreeRatio=5 -XX:MaxHeapFreeRatio=10"
JAJUK_OPTS=""

# Include the wrappers utility script
. /usr/lib/java-wrappers/java-wrappers.sh

# We need a java7 runtime
find_java_runtime java7 java8

exec $JAVA_HOME/bin/java $JAVA_OPTS -jar /usr/share/java/jajuk.jar $JAJUK_OPTS "$@"
