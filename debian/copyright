Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Jajuk
Upstream-Contact: Bertrand Florat <bertrand@florat.net> and The Jajuk Team
Source: http://www.jajuk.info/downloads.html
Files-Excluded: lib/*
                src/packaging/OSX/universalJavaApplicationStub
                src/scripts/nsisant-1.3.jar
                src/main/java/ext/scrollablepopupmenu/*
                src/test/java/ext/scrollablepopupmenu/*
                src/main/java/ext/JScrollingText.java
                src/test/java/ext/TestJScrollingText.java

Files: *
Copyright: © 2003-2010 The Jajuk Team
License: GPL-2+

Files: debian/*
Copyright: 2007-2009, Varun Hiremath <varunhiremath@gmail.com>
           2009-2010, Damien Raude-Morvan <drazzib@debian.org>
           2016 Felix Natter <fnatter@gmx.net>
License: GPL-2+

Files: src/main/java/ext/MenuScroller.java
Copyright: Copyright 2012 JMeter project
License: Apache-2.0

Files: src/main/java/ext/FlowScrollPanel.java
Copyright: Copyright (c) 2001 Regents of the University of California.
License: BSD-4-clause

Files: src/main/java/ext/DropDownButton.java
Copyright: 2005 Santhosh Kumar <santhosh.tekuri@gmail.com>
License: Apache-2.0

Files: src/main/resources/org/jajuk/i18n/*
Copyright: Copyright 2003,2009 Bertrand Florat
 Copyright 2003-2008 Gerhard Dietrichsteiner and Dominik Stadler
 Copyright 2004,2005,2006 Josep Carles Collazos
 Copyright 2006 Bart Cremers
 Copyright 2007, 2008, 2009 Vitaliy Tamm
 Copyright 2008 The Jajuk Team
 Copyright 2008 Xabier Cancela
 Copyright 2009 Zdenek Pech
License: GPL-2+

Files: src/main/java/ext/JSplashLabel.java
 src/main/java/ext/ProcessLauncher.java
 src/main/java/ext/AutoCompleteDocument.java
 src/main/java/ext/JVM.java
 src/main/java/ext/AutoCompleteDecorator.java
 src/main/java/ext/JXTrayIcon.java
 src/main/java/ext/JSplash.java
Copyright: Copyright © 2007 The Jajuk Team
 Copyright (c) 2004,2005 Gregory Kotsaftis <gregkotsaftis@yahoo.com>
 Copyright (C) 2006  Fabio MARAZZATO, Yann D'ISANTO
 Copyright © 2004, 2006, 2008 Sun Microsystems, Inc.
License: LGPL-2.1+

Files: src/main/java/ext/services/lastfm/*
 src/main/java/ext/services/network/NetworkUtils.java
 src/main/java/ext/services/network/Proxy.java
 src/main/java/ext/services/xml/XMLBuilder.java
 src/main/java/ext/services/xml/XMLUtils.java
Copyright: Copyright (C) 2006-2009 Alex Aranda, Sylvain Gaudard, Thomas Beckers and contributors
 Copyright (C) 2006-2007 Alex Aranda (fleax) alex@atunes.org
 Copyright (C) 2003-2008 The Jajuk Team
License: GPL-2+

Files: src/main/resources/icons/*
Copyright: public domain
License: public-domain
 From <http://tango.freedesktop.org/Tango_Desktop_Project>
 The Tango base icon theme is released to the Public Domain.
 The palette is in public domain.
 Developers, feel free to ship it along with your application.
 The icon naming utilities are licensed under the GPL.
 Though the tango-icon-theme package is released to the Public Domain,
 we ask that you still please attribute the Tango Desktop Project,
 for all the hard work we've done. Thanks.

License: GPL-2+
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 .
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 On Debian systems, the complete text of LGPL 2.1 license can be found
 in `/usr/share/common-licenses/LGPL-2.1'.

License: BSD-4-clause
 Copyright (c) 1993 The Regents of the University of California. All
 rights reserved.
 .
 This software was developed by the Computer Systems Engineering group
 at Lawrence Berkeley Laboratory under DARPA contract BG 91-66 and
 contributed to Berkeley.
 .
 All advertising materials mentioning features or use of this software
 must display the following acknowledgement: This product includes
 software developed by the University of California, Lawrence Berkeley
 Laboratory.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
    1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
    2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
    3. All advertising materials mentioning features or use of this
    software must display the following acknowledgement: This product
    includes software developed by the University of California,
    Berkeley and its contributors.
 .
    4. Neither the name of the University nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS''
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Apache-2.0
 On Debian systems, the complete text of Apache-2.0 license can be found
 in `/usr/share/common-licenses/Apache-2.0'.
